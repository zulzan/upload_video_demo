//
//  main.m
//  Demo_Upload_Video
//
//  Created by Anuj Vashistha on 12/07/13.
//  Copyright (c) 2013 Anuj Vashistha. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
