//
//  ViewController.m
//  Demo_Upload_Video
//
//  Created by Anuj Vashistha on 12/07/13.
//  Copyright (c) 2013 Anuj Vashistha. All rights reserved.
//

#import "ViewController.h"
#import "ASIFormDataRequest.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerItem.h>
#import <CoreMedia/CoreMedia.h>
#import "AFURLRequestSerialization.h"
#import "AFURLSessionManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    progressIndicator.hidden =YES;
    [super viewWillAppear:animated];
}

#pragma UIImagePicker Method:::
// For responding to the user tapping Cancel.
- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)ChooseFromGallery:(id)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentModalViewController:imagePicker animated:YES];
}

- (IBAction)RecordAndPlay:(id)sender {
    [self startCameraControllerFromViewController: self
                                    usingDelegate: self];
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    
    [controller presentViewController:cameraUI animated:YES completion:nil];
    return YES;
}

// For responding to the user accepting a newly-captured picture or movie
- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self progressIndicatorView];
    self.ChooseFromGallery.hidden =YES;
    NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
    NSLog(@"urlvideo is :::%@",urlvideo);
    
    NSError *error = nil;
    NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:urlvideo.path error:&error];
    NSNumber * size = [properties objectForKey: NSFileSize];
    NSLog(@"size: %@", size);
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:urlvideo];
    CMTime duration = playerItem.duration;
    float seconds = CMTimeGetSeconds(duration);
    NSLog(@"duration: %.2f", seconds);
    
    /*urlvideo contains the URL of that video file that has to be uploaded. Then convert the url into NSString type because setFile method requires NSString as a parameter
     */
    NSString *urlString=[urlvideo path];
    
    NSString *username = @"xxxxxx";
    NSString *password = @"xxxxxx";
    
    NSString *videoName = urlString.lastPathComponent;
    NSLog(@"video path %@",urlString);
    
    NSLog(@"videoName %@",videoName);
    progressIndicator.hidden=NO;
    _progressLabel.hidden = NO;
    NSString *urlpath = [NSString stringWithFormat:@"http://codinghomes.com/wsvideo/uploadvideo.php"];
    
    NSData *videoData = [NSData dataWithContentsOfFile:urlString];
     NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer]
                                     multipartFormRequestWithMethod:@"POST" URLString:@"http://codinghomes.com/wsvideo/uploadvideo.php"  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                         
                                         
        [formData appendPartWithFileData:videoData name:@"file" fileName:@"video" mimeType:@"video/quicktime"];
                                         
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURLSessionUploadTask *uploadTask;
    
    
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          [progressIndicator setProgress:uploadProgress.fractionCompleted];
                          NSLog(@"uploadProgress.fractionCompleted %f",uploadProgress.fractionCompleted);
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"Success -->%@ \n Responce Object -->%@", response, responseObject);
                          
                          progressIndicator.hidden=YES;
                          _progressLabel.hidden=YES;
                      }
                  }];
    
    [uploadTask resume];
    
    
    
    /* urlpath = [urlpath stringByAppendingString:@"username="];
    urlpath = [urlpath stringByAppendingString:username];
    urlpath = [urlpath stringByAppendingString:@"&password="];
    urlpath = [urlpath stringByAppendingString:password]; */
    
    
     /* NSURL *url = [NSURL URLWithString:[urlpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    request = [ASIFormDataRequest requestWithURL:url];
    
    [request setPostValue:videoName forKey:@"Filename"];
    [request setFile:urlString forKey:@"video"];
    [request setRequestMethod:@"POST"];
    [request setDelegate:self];
    [request setDidStartSelector:@selector(requestStarted:)];
    [request setDidFinishSelector:@selector(requestFinished:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setUploadProgressDelegate:self];
    [request setTimeOutSeconds:50000];
    [request startAsynchronous];
    NSLog(@"url %@",[request requestCredentials]);


    NSLog(@"responseStatusCode %i",[request responseStatusCode]);
    NSLog(@"responseStatusCode %@",[request responseString]); */
}

- (void)requestStarted:(ASIHTTPRequest *)theRequest {
    NSLog(@"response started new::%@",[theRequest responseString]);
    //[self showProgress];
}

- (void)requestFinished:(ASIHTTPRequest *)theRequest {
    NSLog(@"response finished new ::%@",[theRequest responseString]);
    //[self hideProgress];
    progressIndicator.hidden = YES;
    [aProgressView removeFromSuperview];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Video upload to server successfully!"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    self.ChooseFromGallery.hidden =NO;
}


- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    NSLog(@"response Failed new ::%@, Error:%@",[theRequest responseString],[theRequest error]);
    //[self hideProgress];
    progressIndicator.hidden = YES;
    [aProgressView removeFromSuperview];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Video Upload to server failed, please try again"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    self.ChooseFromGallery.hidden =NO;
    
}

#pragma mark Upload Progress Tracking


- (void)setProgress:(float)newProgress {
    
    [progressIndicator setProgress:newProgress];
    NSString* formattedNumber = [NSString stringWithFormat:@"%.f %@", [progressIndicator progress]*100, @"%"];
    
    self.progressLabel.text = formattedNumber;
    self.progressLabel.textColor = [UIColor blackColor];
    [self.progressLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    
}

- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Save failed, please try again"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}


#pragma UserDefined Method:::
-(void)progressIndicatorView{
    
    //self.Record_button.hidden =YES;
    CGRect UploadProgressFrame = CGRectMake(60, 150, 200,100);
    aProgressView = [[UIView alloc] initWithFrame:UploadProgressFrame];
    [aProgressView setBackgroundColor:[UIColor whiteColor]];
    [aProgressView.layer setCornerRadius:10.0f];
    [aProgressView.layer setBorderWidth:1.0f];
    [aProgressView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    UILabel *progressTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 25, 150, 30)];
    progressTitleLabel.backgroundColor = [UIColor whiteColor];
    [progressTitleLabel setFont:[UIFont fontWithName:@"Arial" size:15]];
    progressTitleLabel.text = @"Uploading Video";
    progressTitleLabel.textColor = [UIColor blackColor];
    
    self.progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, 80, 70, 15)];
    [self.progressLabel setBackgroundColor:[UIColor whiteColor]];
    progressIndicator = [[UIProgressView alloc] init];
    progressIndicator.frame = CGRectMake(30,65,140,20);
    [aProgressView addSubview:progressTitleLabel];
    [aProgressView addSubview:self.progressLabel];
    [aProgressView addSubview:progressIndicator];
    [self.view addSubview:aProgressView];
    
}


#pragma UIProgressBar Method::
-(void)showProgress
{
    if (!aMBProgressHUD)
        aMBProgressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    
    [self.view addSubview:aMBProgressHUD];
    aMBProgressHUD.labelText = @"Uploading Video";
    [aMBProgressHUD show:YES];
}

-(void)hideProgress
{
    [aMBProgressHUD hide:YES];
    [aMBProgressHUD removeFromSuperview];
    aMBProgressHUD=nil;
}

- (void)viewDidUnload {
    [self setChooseFromGallery:nil];
    [self setRecordAndPlay:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
