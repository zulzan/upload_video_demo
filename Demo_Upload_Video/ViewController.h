//
//  ViewController.h
//  Demo_Upload_Video
//
//  Created by Anuj Vashistha on 12/07/13.
//  Copyright (c) 2013 Anuj Vashistha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ASIFormDataRequest.h"

@interface ViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIView * aProgressView ;
    UIProgressView *progressIndicator;
    MBProgressHUD *aMBProgressHUD;
 //   ASIFormDataRequest *request;
    
}
- (IBAction)ChooseFromGallery:(id)sender;
- (IBAction)RecordAndPlay:(id)sender;
- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate;
- (void)video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo:(void*)contextInfo;

@property (strong, nonatomic) IBOutlet UIButton *ChooseFromGallery;
@property (strong, nonatomic) IBOutlet UIButton *RecordAndPlay;
@property (strong,nonatomic) IBOutlet UILabel *progressLabel;
//@property(strong,nonatomic)IBOutlet  UIProgressView *progressIndicator;

@end
