//
//  AppDelegate.h
//  Demo_Upload_Video
//
//  Created by Anuj Vashistha on 12/07/13.
//  Copyright (c) 2013 Anuj Vashistha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
